<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-10 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->