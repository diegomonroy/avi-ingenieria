<?php
$style = '';
if ( is_page( array( 'nosotros' ) ) ) : $style = 'nosotros'; endif;
?>
<!-- Begin Content -->
	<section class="content <?php echo $style; ?>" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->