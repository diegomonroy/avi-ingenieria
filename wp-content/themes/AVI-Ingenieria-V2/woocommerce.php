<?php get_header(); ?>
	<main class="main_container">
		<left class="left_container">
			<?php get_template_part( 'part', 'left' ); ?>
		</left>
		<right class="right_container">
			<?php get_template_part( 'part', 'woocommerce' ); ?>
		</right>
	</main>
<?php get_footer(); ?>