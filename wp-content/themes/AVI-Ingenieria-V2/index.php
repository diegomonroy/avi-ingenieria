<?php get_header(); ?>
	<main class="main_container">
		<left class="left_container">
			<?php get_template_part( 'part', 'left' ); ?>
		</left>
		<right class="right_container">
			<!-- Begin Top -->
				<section class="top show-for-small-only" data-wow-delay="0.5s">
					<div class="row collapse align-center align-middle">
						<div class="small-12 columns">
							<?php dynamic_sidebar( 'logo' ); ?>
							<?php dynamic_sidebar( 'menu_mobile' ); ?>
						</div>
					</div>
				</section>
			<!-- End Top -->
			<?php
			if ( is_front_page() ) :
				get_template_part( 'part', 'banner' );
			else :
				get_template_part( 'part', 'content' );
			endif;
			?>
		</right>
	</main>
<?php get_footer(); ?>