<!-- Begin Left -->
	<section class="left" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'copyright' ); ?>
			</div>
		</div>
	</section>
<!-- End Left -->