<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->